# avalon_A10_A11_zabbix

Get cgminer-api data A10/A11 (fan/temp/GHSmm/pool) support universal api

example commands

```
# fan1
echo '{"command":"stats"}' | nc 192.168.168.168 4028 | sed 's/}{/},{/' | sed 's/\x0//g'| jq '.STATS[0]."MM ID0"'|sed 's/\"//g'| awk '{ print $30 }'
# fan2
echo '{"command":"stats"}' | nc 192.168.168.168 4028 | sed 's/}{/},{/' | sed 's/\x0//g'| jq '.STATS[0]."MM ID0"'|sed 's/\"//g'| awk '{ print $31 }'
# fan3
echo '{"command":"stats"}' | nc 192.168.168.168 4028 | sed 's/}{/},{/' | sed 's/\x0//g'| jq '.STATS[0]."MM ID0"'|sed 's/\"//g'| awk '{ print $32 }'
# fan4
echo '{"command":"stats"}' | nc 192.168.168.168 4028 | sed 's/}{/},{/' | sed 's/\x0//g'| jq '.STATS[0]."MM ID0"'|sed 's/\"//g'| awk '{ print $33 }'
#temp
echo '{"command":"stats"}' | nc 192.168.106.196 4028 | sed 's/}{/},{/' | sed 's/\x0//g'| jq '.STATS[0]."MM ID0"'|sed 's/\"//g'| awk '{ print $28 }'
# GHSmm
echo '{"command":"stats"}' | nc 192.168.168.168 4028 | sed 's/}{/},{/' | sed 's/\x0//g'| jq '.STATS[0]."MM ID0"'|sed 's/\"//g'| awk '{ print $54 }'
# url pool
echo '{"command":"pools"}' | nc 192.168.168.168 4028|sed 's/}{/},{/' | sed 's/\x0//g'| jq '.POOLS[0]."Stratum URL"'
# worker
echo '{"command":"pools"}' | nc 192.168.168.168 4028|sed 's/}{/},{/' | sed 's/\x0//g'| jq '.POOLS[0]."User"'
```
example run avalon10-zabbix-check
```
./avalon10-zabbix-check 192.168.106.196 4028 stats GHSmm | cut -c 7-8
```
